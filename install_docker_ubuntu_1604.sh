#!/bin/bash

echo "Updating apt sources..."
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificate
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list

echo "Installing prerequisites..."
sudo apt-get update
sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
sudo apt-get update

echo "Installing docker..."
sudo apt-get install docker-engine

echo "Configuring docker to start on boot..."
sudo systemctl enable docker

echo "Adding group 'docker'..."
sudo groupadd docker
echo "Adding current user to group 'docker'..."
sudo usermod -aG docker $USER
echo "NOTE: You need to log out and log back in to use docker without sudo."

echo "Successfully installed and configured docker!"
