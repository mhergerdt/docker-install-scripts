#!/bin/bash

echo "Downloading docker-compose..."
curl -L "https://github.com/docker/compose/releases/download/1.8.1/docker-compose-$(uname -s)-$(uname -m)" > ./docker-compose
sudo mv ./docker-compose /usr/local/bin/docker-compose
echo "Setting up permissions..."
sudo chmod +x /usr/local/bin/docker-compose
echo "Successfully installed docker-compose!"
docker-compose --version
